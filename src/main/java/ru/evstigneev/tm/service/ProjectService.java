package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.enumerated.Status;
import ru.evstigneev.tm.repository.IProjectRepository;
import ru.evstigneev.tm.util.DateParser;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

@Service
@Transactional
public class ProjectService {

    @Autowired
    private IProjectRepository projectRepository;

    @Transactional
    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }

    @Transactional
    public void create(@NotNull final String projectName,
                       @NotNull final String description) {
        if (projectName.isEmpty() || description.isEmpty()) {
            throw new IllegalArgumentException();
        }
        @NotNull final Project project = new Project();
        project.setId(UUID.randomUUID().toString());
        project.setName(projectName);
        project.setDescription(description);
        project.setDateOfCreation(new Date());
        project.setStatus(Status.PLANNING);
        projectRepository.save(project);
    }

    @Transactional
    public void persist(@NotNull final Project project) {
        projectRepository.save(project);
    }

    @Transactional
    public void remove(@NotNull final String projectId) throws Exception {
        if (projectId.isEmpty()) {
            throw new IllegalArgumentException();
        }
        projectRepository.delete(findOne(projectId));
    }

    @Transactional
    public void update(@NotNull final String projectId,
                       @NotNull final String name, @Nullable final String description,
                       @Nullable final String dateStart, @Nullable final String dateFinish,
                       @NotNull final Status status) throws Exception {
        if (projectId.isEmpty() || name.isEmpty() || description == null || description.isEmpty()) {
            throw new IllegalArgumentException();
        }
        projectRepository.update(projectId, name, description, DateParser.setDateByString(dateStart),
                DateParser.setDateByString(dateFinish), status);
    }

    @Transactional
    public Project findOne(@NotNull final String projectId) throws Exception {
        if (projectId.isEmpty()) {
            throw new IllegalArgumentException();
        }
        return projectRepository.findById(projectId).orElse(null);
    }

    @Transactional
    public void removeAll() {
        projectRepository.deleteAll();
    }

}
