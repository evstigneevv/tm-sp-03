package ru.evstigneev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.enumerated.Status;
import ru.evstigneev.tm.service.ProjectService;
import ru.evstigneev.tm.service.TaskService;

import java.util.Arrays;
import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;
    @Autowired
    private ProjectService projectService;

    @RequestMapping(value = "/taskList/{id}", method = RequestMethod.GET)
    public String getProjectTask(@PathVariable final String id, @NotNull final Model model) throws Exception {
        @NotNull final Collection<Task> taskList = taskService.findAllByProject(id);
        model.addAttribute("tasks", taskList);
        return "taskList";
    }

    @RequestMapping(value = "/taskList", method = RequestMethod.GET)
    public String getTaskList(@NotNull final Model model) {
        @NotNull final Collection<Task> taskList = taskService.findAll();
        model.addAttribute("tasks", taskList);
        return "taskList";
    }

    @RequestMapping(value = "/taskView/{id}", method = RequestMethod.GET)
    public String getTask(@PathVariable final String id, @NotNull final Model model) throws Exception {
        @NotNull final Task task = taskService.findOne(id);
        model.addAttribute("task", task);
        return "taskView";
    }

    @RequestMapping(value = "/taskCreate/{projectId}", method = RequestMethod.GET)
    public String createTaskGet(@PathVariable final String projectId, @ModelAttribute("task") final Task task) throws Exception {
        task.setProject(projectService.findOne(projectId));
        return "taskCreate";
    }

    @RequestMapping(value = "/taskCreate/{projectId}", method = RequestMethod.POST)
    public String createTaskPost(@PathVariable final String projectId,
                                 @ModelAttribute("name") final String name,
                                 @ModelAttribute("description") final String description) throws Exception {
        taskService.create(projectService.findOne(projectId), name, description);
        return "redirect:/projectView/{projectId}";
    }

    @RequestMapping(value = "/taskDelete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable final String id) throws Exception {
        taskService.remove(id);
        return "redirect:/projectList";
    }

    @PostMapping(value = "/taskEdit")
    public String updateProjectPost(@ModelAttribute("id") final String id,
                                    @ModelAttribute("name") final String name,
                                    @ModelAttribute("description") final String description,
                                    @ModelAttribute("dateStart") final String dateStart,
                                    @ModelAttribute("dateFinish") final String dateFinish,
                                    @ModelAttribute("status") final Status status) throws Exception {
        taskService.update(id, name, description, dateStart, dateFinish, status);
        return "redirect:/projectList";
    }

    @GetMapping(value = "/taskEdit/{id}")
    public String updateProjectGet(@PathVariable final String id, @NotNull final Model model) throws Exception {
        @NotNull final Task task = taskService.findOne(id);
        model.addAttribute("task", task);
        model.addAttribute("status", Arrays.asList(Status.values()));
        return "taskEdit";
    }

}