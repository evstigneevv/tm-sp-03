package ru.evstigneev.tm.entity;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "projects")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Cacheable
public class Project {

    @Id
    @NotNull
    private String id;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @Column(name = "date_of_creation")
    @NotNull
    private Date dateOfCreation;

    @Column(name = "date_start")
    @Nullable
    private Date dateStart;

    @Column(name = "date_finish")
    @Nullable
    private Date dateFinish;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Status status;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project", cascade = CascadeType.REMOVE)
    @Nullable
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private List<Task> tasks;

    @NotNull
    public String getId() {
        return id;
    }

    public void setId(@NotNull final String id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(@NotNull final String name) {
        this.name = name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable final String description) {
        this.description = description;
    }

    @NotNull
    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(@NotNull final Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    @Nullable
    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(@Nullable final Date dateStart) {
        this.dateStart = dateStart;
    }

    @Nullable
    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(@Nullable final Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @NotNull
    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull final Status status) {
        this.status = status;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(@NotNull final List<Task> tasks) {
        this.tasks = tasks;
    }

    @Override
    public String toString() {
        return "Project ID: " + getId() + " | Project name: "
                + getName() + " | Project description: " + getDescription() + " | Date of creation: "
                + getDateOfCreation() + " | Date of start: " + getDateStart() + " | Date of finish: "
                + getDateFinish() + " | Project status: " + getStatus();
    }

}