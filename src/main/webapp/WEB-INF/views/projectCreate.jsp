<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 11.02.2020
  Time: 17:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Create project</title>
</head>
<%@include file="navbar.jsp" %>
<body>
<form:form method="post" action="${pageContext.request.contextPath}/projectCreate" modelAttribute="project">
    <table>
        <tr>
            <td>Project name:</td>
            <td><form:input path="name"/></td>
        </tr>
        <tr>
            <td>Project description:</td>
            <td><form:input path="description"/></td>
        </tr>
    </table>
    <button type="submit" class="new">Create Project</button>
</form:form>
</body>
</html>
