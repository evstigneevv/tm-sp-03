<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>Project List</title>
</head>
<%@include file="navbar.jsp" %>
<body>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>Project id</th>
        <th>Project name</th>
        <th>Project description</th>
        <th>View</th>
        <th>Edit</th>
        <th>Remove</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td><c:out value="${project.id}"/></td>
            <td><c:out value="${project.name}"/></td>
            <td><c:out value="${project.description}"/></td>
            <td><a href="${pageContext.request.contextPath}/projectView/${project.id}">View</a></td>
            <td><a href="${pageContext.request.contextPath}/projectEdit/${project.id}">Edit</a></td>
            <td><a href="${pageContext.request.contextPath}/projectDelete/${project.id}">Remove</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<a class="btn btn-primary" href="${pageContext.request.contextPath}/projectCreate" role="button">Create project</a>
<a class="btn btn-primary" href="${pageContext.request.contextPath}/projectList" role="button">Refresh</a>
</body>
</html>
